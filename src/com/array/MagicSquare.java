package com.array;
import java.util.Scanner;

public class MagicSquare {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int lengthOfSquareGrid = scanner.nextInt();
        int[][] squareGrid = new int[lengthOfSquareGrid][lengthOfSquareGrid];
        for (int i = 0; i < lengthOfSquareGrid; i++) {
            for (int j = 0; j < lengthOfSquareGrid; j++) {
                squareGrid[i][j] = sc.nextInt();
            }
        }
        System.out.println(CheckMagicSquare(squareGrid));
    }

    private static String CheckMagicSquare(int[][] squareGrid) {
        int diagonal1 = 0, diagonal2 = 0;
        for (int i = 0; i < squareGrid[0].length; i++) {
            diagonal1 += squareGrid[i][i];
            diagonal2 += squareGrid[i][squareGrid[0].length - 1 - i];
        }
        if (diagonal1 != diagonal2)
            return "No";

        for (int i = 0; i < squareGrid[0].length; i++) {

            int rowSum = 0, columnSum = 0;
            for (int j = 0; j < squareGrid[0].length; j++) {
                rowSum += squareGrid[i][j];
                columnSum += squareGrid[j][i];
            }
            if (rowSum != columnSum || columnSum != diagonal1)
                return "No";
        }
        return "Yes";
    }
}
